# README #

This is the solution for the problem posted in http://www.dragonsofmugloar.com/api. The solution is implemented in nodejs and php

## How to run the program? ##
	
	1. Clone the project

* NodeJS
	
	2. cd to the project root
	3. npm install
	4. node app.js (The node server will start listening to port 9000)
	5. On the browser, go to url : http://localhost:9000/battle/<no of battles>

	The information about battle will be shown in the browser, a log file will be created in the same directory

* PHP
	2. cd to project root
	3. php battle.php <no of battles>

	The information about the battle will be shown in the terminal.

### Why two different set of languages are used? ###

Nodejs's asynchronus callbacks made it difficult to track which battle results are being returned by the server. So assocating the battle results with the gameId was not achieveable in the planned time frame (more effort was not put into this). Implementing the same alogrithm in a different / synchronus  programming language like PHP would 
also provide a comparision of efficiency of both the languages. Plus, the logging problem faced in NodeJS would be solved easily in php (but at a cost of execution time)

### Dependencies ###

* NodeJS dependencies can be found in the package.json file
* CURL for PHP: In PHP the curl requests are made via shell, which turned out to be more effecient and less lines of code than PHP's native curl methods
* Also, the code can only be executed in linux / unix programs with curl installed


### What is not done? ###

* The weather factor is not considered. The algorithm used in the programs above give a 100% result when weather is not considered. For the give data sets (from the API) a success rate of more than ## 80% ## was achieved in 10, 100, 500 and 1000 iterations using this algorithm for ## NodeJS ##, without including the weather conditions. All the defeats were due to the weather conditions only. But since it is said as optional feature and the program meets the requirement without using weather data, weather data was not used. More than 1000 iterations were not tried. PHP was more like a way to demonstrate the conceived idea of logging. A max of 100 iterations were tried in PHP which was way more ineffecient than NodeJS.

Example:

Iterations       Language      Execution time (seconds)
	100				Node 		113.259 
	100				php 		227.351