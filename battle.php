<?php 
	echo "Application running! Please wait, it will take a while depending upon the number of battles.... \n\n";
	if(count($argv) != 2)
		die("Incorrect parameters! Correct syntax: php battle.php <no of battles>\n");
	//marks the start of execution time
	$start = microtime(true);
	//opening the file only once in append mode. The same handle will be used everytime to write / append to the file when in loop
	$logFileName	=	'php-battlelog.log';
	//if log file exists, delete it so that a fresh file is created everytime that will log the current battle
	if(file_exists($logFileName))
		unlink($logFileName);
	$battleLogFile	=	fopen($logFileName,'a') or die('Error opening file!');
	
	$noOfSuccessfulBattles = 0;
	$noOfBattles = 0;
	while($noOfBattles < $argv[1]):
		$knight = json_decode(shell_exec("curl -s --request GET \"http://www.dragonsofmugloar.com/api/game\""),true); //executiion time: 0.027665348847707
		//It was found after benchmarking that executing curl via shell command method in php is sweeter (few lines of code) and swifter comparted to curl or file_get_contents method
		/*$knight = json_decode(file_get_contents("http://www.dragonsofmugloar.com/api/game")); execution time: 0.010346750418345  */
		
		arsort($knight["knight"]);
		array_pop($knight['knight']);

		$attributeMap = array("attack" => "scaleThickness", "armor" => "clawSharpness", "agility" => "wingStrength" ,"endurance" => "fireBreath");
		$dragon = array();
		foreach($knight["knight"] as $key => $value):
			$dragon[$attributeMap[$key]] = $knight["knight"][$key];
		endforeach;

		/*Since the forloop here sets the internal pointer of knight and dragon to last indices, they are resetted to initial position in the lines below*/
		reset($dragon);
		reset($knight["knight"]);

		/* The algorithm same as nodejs is applied */
		$dragon[key($dragon)]	=	current($knight["knight"]) + 2;
		$count = 1;

		while($count < count($dragon)):
			next($dragon);
			next($knight["knight"]);
			if($count == 1 || $count == 2 && $count != 3)
				$dragon[key($dragon)] 	=	(current($knight["knight"]) - 1 < 0) ? 0 :  current($knight["knight"]) - 1 ;
			else
				 $dragon[key($dragon)]	=	current($knight["knight"]);
			$count++;
		endwhile;	
		
		$dragonArray = json_decode(json_encode($dragon),true);
		$dragonJSON = json_encode($dragon);
		$result[] = json_decode(shell_exec("curl -s -H 'Content-Type: application/json' -X PUT -d '{\"dragon\":".$dragonJSON."}' http://www.dragonsofmugloar.com/api/game/".$knight['gameId']."/solution"),true);
		
		//putting knights and dragons along with the battle results
		$result[count($result)-1]['knight'] = $knight;
		$result[count($result)-1]['dragon'] = $dragon;
		$result[count($result)-1]['timestamp'] = date('Y-m-d G:i:s');
		//if status in $result = Victory then update the noOfSuccessfulBattles counter
		if($result[count($result)-1]['status'] == "Victory")
			$noOfSuccessfulBattles++;
		//write the results to the logFile that is opened in append mode before the start of the loop
		fwrite($battleLogFile,json_encode($result));
		//write nextline in the logFile to make the logs readable
		fwrite($battleLogFile,"\r\n");
		$noOfBattles++;
	endwhile;
	fclose($battleLogFile);
	//logging the battle
	$end = microtime(true);

	echo "Total exeution time: ".($end-$start)." seconds \n noOfBattles: ".$noOfBattles."\n noOfSuccessfulBattles: ".$noOfSuccessfulBattles." \nSuccessful Battle percentage: ".(($noOfSuccessfulBattles/$noOfBattles) * 100)."\n";


?>