//using nodejs in-built function to calculate the total execution time.Starting the execution time tracker
console.time("ex-time");
var express = require('express');
var app = express();
const request = require('request');
const fs = require('fs');
//using moment js for time stamps. Adding timestamp to the result json object received from server increased the execution time by around 300 milliseconds
var moment = require('moment'); 

const logFile = 'battle.log';
//create a new logfile for every server restart, so that the most recent battles can be logged. The log file is created in the same directory as the app.js file
fs.writeFile(logFile,""); 
var battle = {}

// this attributeMap is used to map the attributes of the knights with that of dragons to conveniently assign additional points to the dragon's attributes
// that is mapped to the more powerful attributes of Knights 
var attributeMap = {"attack" : "scaleThickness", "armor" : "clawSharpness", "agility" : "wingStrength" ,"endurance" : "fireBreath"};

app.get('/',function(req,res){

	res.send("The appropriate url format is: http://localhost:9000/battle/<no of times you want the battle to happen>");

});

app.get('/battle/:iteration', function (req, res) {
	console.log("Application running! Please wait, it will take a while depending upon the number of battles.... ");
	var count = 0;
	// below two variables are needed to calculate the percentage of successful battles,
	// since its really hard to track which callback ends when. 
	var successfulBattleCount = 0; 
	var executedCallBacksCount = 0;
	while (count < req.params.iteration){
		//fetch a new game / knight from the server using a get request
		request('http://www.dragonsofmugloar.com/api/game', function (error, response, knight) {
			if (!error && response.statusCode == 200) {
				
				//console.log("knight: \n"+knight+ "\n");
				var test = JSON.parse(knight);
				var knightAttributes = [["attack", test["knight"]["attack"]] , ["armor", test["knight"]["armor"]], ["agility", test["knight"]["agility"]], ["endurance", test["knight"]["endurance"]] ];
				
				//sort knight's attributes in decreasing order
				knightAttributes.sort(function(a, b) {
				    return b[1] - a[1];
				});

				/*console.log("Knight: ");
				console.log(knightAttributes);*/
				var dragon = {};

				//apply the algorithm to prepare the dragon strong enough to defeat the knight
				// in line 10 an attributeMap is created, which associates Knights attribute with Dragon's
				// so Dragon will be created with attributes stronger than the strongest attributes of Knights

				dragon[attributeMap[knightAttributes[0][0]]] = Number(knightAttributes[0][1] + 2);
				dragon[attributeMap[knightAttributes[1][0]]] = (Number(knightAttributes[1][1] - 1) < 0) ? 0 : Number(knightAttributes[1][1] - 1);
				
				// if the weak attributes are 0 then keep them as 0, reduce only when the reduced value is greater than or equal to 0
				dragon[attributeMap[knightAttributes[2][0]]] = (Number(knightAttributes[2][1] - 1) < 0) ? 0 : Number(knightAttributes[2][1] - 1);
				dragon[attributeMap[knightAttributes[3][0]]] = Number(knightAttributes[3][1]);

				/*
				SECOND FORMULA: LESS RELIABLE
				dragon[attributeMap[knightAttributes[0][0]]] = Number(knightAttributes[0][1] + 2);
				dragon[attributeMap[knightAttributes[1][0]]] = Number(knightAttributes[1][1] );
				
				// if the weak attributes are 0 then keep them as 0, reduce only when the reduced value is greater than or equal to 0
				dragon[attributeMap[knightAttributes[2][0]]] = (Number(knightAttributes[2][1] - 1) < 0) ? 0 : Number(knightAttributes[2][1] - 1);
				dragon[attributeMap[knightAttributes[3][0]]] = (Number(knightAttributes[3][1] - 1) < 0) ? 0 : Number(knightAttributes[3][1] - 1);*/

				/*console.log("Dragon: ");
				console.log(dragon);*/
				
				//makes a put request to the server with the newly created dragon
				request({ 
					url: "http://www.dragonsofmugloar.com/api/game/"+test['gameId']+"/solution", 
					method: 'PUT', 
					json: {"dragon": dragon } } , 
					function(error, request, battleResult){					
						/*console.log("Battle Result: ");*/
						//injecting timestamp in the received JSON Object for loggin purpose
						if(error){

							console.log('undefined');
							return error;
						}

						battleResult.timeStamp = moment().format('MMMM Do YYYY, h:mm:ss a');
						/*console.log(battleResult);*/
						appendFile(logFile,JSON.stringify(battleResult));
						// this portion of code is needed to calculate the percentage of success in battle
						// since its a callback hell, to keep track of the callbacks and the successful battle, these counters are needed
						executedCallBacksCount++;
						
						if(battleResult["status"] == "Victory")
							successfulBattleCount++;

						if(executedCallBacksCount == ( count )){
							var percentSuccess = (successfulBattleCount / count ) * 100;
							console.log("Successful battle: "+percentSuccess+" %");
							// ending the execution time tracker
							console.timeEnd("ex-time");
							res.send("Successful battle: "+percentSuccess+" % successful battles: "+successfulBattleCount);
						}
					}
				);	 
			}
		});
		count++;
	}

});

//this function is needed for manual check of values. Only for test purpose
app.get('/single',function(req,res){
	
	var dragon = { "dragon": { scaleThickness :  10, clawSharpness :  0 , wingStrength:  5 , fireBreath:  5 } };
	console.log(JSON.stringify(dragon));
	request({ 
				url: "http://www.dragonsofmugloar.com/api/game/7150257/solution", 
				method: 'PUT', 
				json: dragon } , 
				function(error, request, battleResult){			
					/*appendFile(logFile,game);
					appendFile(logFile,dragon);
					appendFile(logFile,battleResult);*/
					console.log(battleResult);
				}
			);	 
});

//function to append the battle results in a log file.
function appendFile(location, data){
	fs.appendFile(location, data + "\r\n", function(err, fd) {
		if (err) 
			return console.error(err);
		console.log('Battle info written correctly!');
	});
}

//node server is listening to port 9000
app.listen(9000, function () {
	console.log('Example app listening on port 9000!');
});



